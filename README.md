# Ruoyi-Dept-Export
#### 介绍
Ruoyi-Dept-Export项目简称RDE,是一款专用的【部门组织架构图导出】解决方案项目。
#### 软件架构
项目依赖于若依框架前后端分离版（springboot+vue+mysql)
主要使用到的表：用户表、岗位表、部门表
#### 体验环境链接：http://43.134.134.21:81/system/dept
#### 体验环境链接：http://43.134.134.21:81/system/dept
#### 体验环境链接：http://43.134.134.21:81/system/dept
#### 导出的EXCEL效果图
![组织架构图导出效果](https://gitee.com/jacklongma/ruoyi-dept-export/raw/master/group.png)
#### 咨询或合作微信联系：
![联系人](https://gitee.com/jacklongma/ruoyi-dept-export/raw/master/wechat.jpg)



